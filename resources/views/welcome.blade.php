@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Upload a CSV File') }}</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('welcome') }}" enctype="multipart/form-data">

                            @csrf

                            <div class="form-group row">

                                <label for="email"
                                       class="col-md-3 col-form-label">{{ __('CSV File') }}</label>

                                <div class="col-md-6">
                                    <input id="image"
                                           type="file"
                                           class="form-control @error('image') is-invalid @enderror"
                                           name="image"
                                           value="{{ old('image') }}" accept=".csv" style="border: none">

                                    {{-- accept=".csv"--}}

                                    @error('image')

                                    <span class="invalid-feedback" role="alert"
                                          style="margin-top: 15px;font-size: 15px;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <hr>


                            <div class="form-group row mb-0">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Upload') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
