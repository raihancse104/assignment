<?php

namespace App\Http\Controllers;

use App\Repositories\CsvUploadInterface;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $csvUpload;

    public function __construct(CsvUploadInterface $csvUpload)
    {

        $this->csvUpload = $csvUpload;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {

            $request->validate(
                [
                    'image' => 'required|file|mimes:csv,xlsx,xls,txt'
                ],
                [
                    'image.required' => 'Please select e file',
                    'image.mimes' => 'Please select a CSV File'
                ]);


            #ger collection data from CsvUploadRepository
            $collection = $this->csvUpload->uploadCSV($request);

            return response()->json($collection);


        } else {


            return view('welcome');

        }


    }

}
