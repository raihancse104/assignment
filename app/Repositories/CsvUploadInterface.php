<?php

namespace App\Repositories;

interface CsvUploadInterface
{

    public function uploadCSV($request);

}
