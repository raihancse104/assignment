<?php

namespace App\Repositories;

use Rap2hpoutre\FastExcel\FastExcel;

use Illuminate\Filesystem\Filesystem;
use DateTime;

class CsvUploadRepository implements CsvUploadInterface
{

    public function uploadCSV($request)
    {

        #clear all previous files
        $file = new Filesystem;

        $file->cleanDirectory("import/");

        $image = $request->file('image');

        $directory = public_path("import/");

        $filename = date('ymdhis') . '.' . $image->getClientOriginalExtension();

        #image moved to directory
        $image->move($directory, $filename);

        #return collections from csv
        $collection = (new FastExcel)->import($directory . $filename);

        #collection array to format collection data
        $collection_array = [];

        foreach ($collection as $item) {

            #modified excel date as original date format
            $date_format = DateTime::createFromFormat('d/m/y', $item['Date'])->format('Y-m-d');

            #plain date format
            $date_plane = date('Ymd', strtotime($date_format));

            #generate group id accroding to the account, product, plain date
            $group_id = $item['Acct'] . '_' . $item['Product'] . '_' . $date_plane;

            $collection_array[] = [
                "sl" => (int)$item['Sl'],
                "timestamp" => strtotime($date_format . ' ' . $item['Time']),
                "transaction_at" => date('Y-m-d H:i:s', strtotime($date_format . ' ' . $item['Time'])),
                "product" => $item['Product'],
                "Side" => $item['Side'],
                "qty" => (int)$item['Qty'],
                "price" => (float)$item['Exe Price'],
                "acct" => $item['Acct'],
                "group" => $group_id
            ];

        }

        #sorting array according to the timestamp
        usort($collection_array, function ($item1, $item2) {
            return $item1['timestamp'] <=> $item2['timestamp'];
        });


        #set group id based dictionary array
        $group_id_based_dic = [];

        /*
         * loop for formatting group wise data as sub grouping
         */
        foreach ($collection_array as $item) {

            $group_id = $item['group'];

            #check group id index isset or not
            if (isset($group_id_based_dic[$group_id])) {

                $current_array = $group_id_based_dic[$group_id];

                $current_array[] = $item;

                $group_id_based_dic[$group_id] = $current_array;

            } else {

                $group_id_based_dic[$group_id] = [$item];

            }

        }

        $group_by_data_array = [];

        /*
         * loop for formatting final stage to added merge data according to the sold and bot values
         */
        foreach ($group_id_based_dic as $key => $group_array) {

            $balance = -1;
            $group_sub_array = [];
            $sld_array = [];
            $bot_array = [];
            $totalBot = 0;
            $totalSld = 0;

            foreach ($group_array as $group_item) {

                #check item Side value
                if ($group_item['Side'] == "BOT") {

                    $totalBot += (int)$group_item['qty'];

                    $bot_array[] = $group_item;


                } elseif ($group_item['Side'] == "SLD" || $group_item['Side'] == "SLD SHRT") {

                    $totalSld += (int)$group_item['qty'];

                    $sld_array[] = $group_item;

                } else {

                }

                $balance = $totalBot - $totalSld;

                #check balance for total bot and total sld
                if ($balance == 0) {

                    $group_sub_array[] = [
                        'sold' => $sld_array,
                        'bot' => $bot_array,
                        'botQty' => $totalBot,
                        'soldQty' => $totalSld,
                        'closed' => true
                    ];

                    #set initial value after balance equal
                    $sld_array = [];
                    $bot_array = [];
                    $totalBot = 0;
                    $totalSld = 0;

                }

            }

            if (!empty($sld_array) && !empty($bot_array)) {

                $group_sub_array[] = [
                    'sold' => $sld_array,
                    'bot' => $bot_array,
                    'botQty' => $totalBot,
                    'soldQty' => $totalSld,
                    'closed' => false
                ];

            }


            $group_by_data_array[$key] = $group_sub_array;

        }

        #return required data into controller
        return $group_by_data_array;

    }

}
