<?php

namespace App\Providers;

use App\Repository\EloquentRepositoryInterface;
use App\Repositories\CsvUploadInterface;
use App\Repositories\CsvUploadRepository;
use Illuminate\Support\ServiceProvider;
use App\Repository\Eloquent\BaseRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CsvUploadInterface::class, CsvUploadRepository::class);
    }
}
